# appkit

### 介绍
appkit是一个具有可移植性的Linux应用开发库，其目的是为了加快应用程序的开发速度，提升应用程序的健壮性，以解放程序员的大脑和双手，让大家把精力投入到更有意义的事情当中去。

appkit提供了包括:
跟踪打印(Tracer)、线程(Thread)、协程(Coroutine)、定时器(Timer)、文件IO(File)、时间日期(DateTime)、字符串工具(StrUtil)、正则匹配(RegExp)、串口(SerialPort)、网络通信(Socket)、CAN通信(CANSocket)、GPIO控制(PinCtrl)、SPI通信(SpiDev)、插件(Pluglet)、日志(Logger)、有限状态机(FSMachine)、单元测试(CppUnitLite)等Linux应用开发中常用的模块。

在appkit项目中集成了非常多优秀的开源库：
如fmt, json, spdlog, sqlite_orm, yaml-cpp等，感谢这些开源库的作者的无私奉献！

### 作者

有任何问题，欢迎联系： cblock@126.com 牛咕噜大人

### 开源协议

本软件遵循MIT协议,请自觉遵守该协议,如果您使用此源码,请务必保留README在您的工程代码目录下!

### 使用说明

本工程内置zbuild编译系统，完全不依赖于ROS环境，编译方法如下：

	source environ/envsetup.sh
	zbuild_setup # 选择0
    cd zbuild && mkdir build-x64 && cd build-x64
	cmake ..
	make install

也可以直接调用根目录下的脚本进行编译：
	
	./x64-zbuild.sh

本工程也支持ROS环境下编译，默认使用catkin进行编译，编译前请安装ROS(推荐使用Noetic)：

	./x64-build.sh noetic

同时本工程还支持使用交叉编译工具链进行编译，交叉编译环境使用docker构建，请参考:[交叉编译环境构建Dockfile](environ/docker/Dockerfile-x64-ubuntu20-noetic)

	./docker_cmd.sh zbuild 	# 构建aarch64版本的非ROS应用
	./docker_cmd.sh build  	# 构建aarch64版本的ROS应用

非ROS应用编译输出目录为[zbuild/deploy](zbuild/deploy).
ROS应用编译输出目录为[ws_output/deploy](ws_output/deploy).

### 如何运行

以非ROS应用为例:

	cd zbuild/deploy/linux-amd64-host
	source install/setup.bash
	appkit_test
	

### 参与贡献

1.  Fork 本仓库
2.  新建 feature_xxx 分支
3.  提交代码
4.  新建 Pull Request
