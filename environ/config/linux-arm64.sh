#!/bin/bash
export ZBUILD_PLATFORM=linux-arm64
export ZBUILD_PROJECT=cross
export ZBUILD_ARCH=aarch64
export ZBUILD_SYSNAME=Linux
export ZBUILD_ENVIRON=$WS_ROOT/environ
## ZBUILD_CROSS=1 时以下环境变量才有效
export ZBUILD_SYSROOT=/opt/toolchain/linux-aarch64-noetic/sysroot
export ZBUILD_TOOLCHAIN=/usr/bin/
export ZBUILD_BUILDHOST=aarch64-linux-gnu