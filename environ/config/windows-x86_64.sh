#!/bin/bash
export ZBUILD_PLATFORM=windows
export ZBUILD_PROJECT=sdk
export ZBUILD_ARCH=x86_64
export ZBUILD_SYSNAME=Windows
export ZBUILD_ENVIRON=$WS_ROOT/environ
## ZBUILD_CROSS=1 时以下环境变量才有效 
export ZBUILD_SYSROOT=
export ZBUILD_BUILDHOST=x86_64-w64-mingw32
if [ -d $WS_ROOT/toolchain/mingw-w64/x86_64/bin ]; then
export ZBUILD_TOOLCHAIN=$WS_ROOT/toolchain/mingw-w64/x86_64/bin
else
export ZBUILD_TOOLCHAIN=/opt/mingw-w64/x86_64/bin
fi
