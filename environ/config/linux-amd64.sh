#!/bin/bash
export ZBUILD_PLATFORM=linux-amd64
export ZBUILD_PROJECT=host
export ZBUILD_ARCH=x86_64
export ZBUILD_SYSNAME=Linux
export ZBUILD_ENVIRON=$WS_ROOT/environ
## ZBUILD_CROSS=1 时以下环境变量才有效 
export ZBUILD_SYSROOT=
export ZBUILD_BUILDHOST=
export ZBUILD_TOOLCHAIN=