## docker常用操作

### Docker配置
```
vi /etc/docker/daemon.json 
{
    "registry-mirrors" : [
    "https://docker.mirrors.ustc.edu.cn",
    "http://hub-mirror.c.163.com"],
    "insecure-registries": [
    "http://harbregistry.cblock.com"]
}
```

### 构建镜像
```
# docker build . -f environ/docker/Dockerfile-x64-ubuntu20-noetic
```

### 打开容器
```
# docker start <CONTAINER>
# docker attach <CONTAINER>
```

### 运行新容器
```
# docker rm <CONTAINER>
# docker run -it --name <CONTAINER> -v <HOST_PATH>:<CONTAINER_PATH> <IMAGE> /bin/bash
```

### 文件拷贝
```
# docker cp <FILE> <CONTAINER>:<CONTAINER_FILE_PATH>
```

### 更新镜像
```
# docker commit <CONTAINER> <IMAGE>:<IMAGE_TAG>
```

### 打包&解包镜像
```
# docker save -o <TAR_NAME> <IMAGE>:<IMAGE_TAG>
# docker load -i <TAR_NAME>
```

### 压缩镜像
```
# docker-squash -t <NEW_IMAGE>:<NEW_IMAGE_TAG> <OLD_IMAGE>:<OLD_IMAGE_TAG>
```

### 推送镜像
```
# docker login -u <username> -p <password> http://harbregistry.cblock.com/
# docker tag <LOCAL_IMAGE>:<LOCAL_IMAGE_TAG> harbregistry.cblock.com/test/<IMAGE>:<IMAGE_TAG>
# docker push harbregistry.cblock.com/test/<IMAGE>:<IMAGE_TAG>
```
