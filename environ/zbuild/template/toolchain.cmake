###########################################################
# we can use CMAKE_TOOLCHAIN_FILE to set toolchains:
# cmake .. -DCMAKE_TOOLCHAIN_FILE=../toolchain.cmake
###########################################################
# this is required
SET(CMAKE_SYSTEM_NAME Linux)
SET(CMAKE_SYSTEM_PROCESSOR aarch64)

# toolchain path
set(TOOLCHAIN_PATH /opt/toolchain/aarch64/gcc-arm-10.3-2021.07-x86_64-aarch64-none-linux-gnu/bin)
set(HOST_NAME aarch64-none-linux-gnu)

# specify the cross compiler
SET(CMAKE_C_COMPILER ${TOOLCHAIN_PATH}/${HOST_NAME}-gcc)
SET(CMAKE_CXX_COMPILER ${TOOLCHAIN_PATH}/${HOST_NAME}-g++)

# where is the target environment 
SET(CMAKE_FIND_ROOT_PATH "")

# search for programs in the build host directories (not necessary)
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)