#!/bin/bash
# environment setup command
# interactive  : zbuild_setup
# use ROS Build: zbuild_setup <platform_id> <is_release>
# non ROS Build: zbuild_setup <platform_id> <is_release> <zbuild_enable>
function zbuild_setup()
{
    files=$(ls $ZBUILD_ENVIRON/config/*.sh)
    # 在用户定义的配置文件夹中查找配置
    if [ -d "$WS_ROOT/config" ];then
        files+=" "
        files+=$(ls $WS_ROOT/config/*.sh)
    fi
    echo "------------------------------"
    echo "please select configuration:"
    local configuration
    local config
    local i=0
    for filename in $files
    do
        configuration[$i]=$filename
        f=`basename $filename`
        f=$(echo $f | sed 's/\.[^.]*$//')
        echo "  $i --> $f"
        config[$i]=$f
        i=$[$i+1]
    done
    echo "------------------------------"
    if [ $# -ne 0 ];then
        if [ $1 -lt $i ];then
            echo "select configuration: ${config[$1]}"
            selection=$1
        else
            echo "which would you like? Default configuration: ${config[0]}"
            read selection
        fi
    else
        echo "which would you like? Default configuration: ${config[0]}"
        read selection
    fi
    #echo ${configuration[$selection]}
    source ${configuration[$selection]}

    ## 判断是否是交叉编译
    machine=`uname -m`
    if [ x"$machine" = x"$ZBUILD_ARCH" -a x"Linux" = x"$ZBUILD_SYSNAME" ];then
        export ZBUILD_CROSS=0
        unset ZBUILD_SYSROOT
        unset ZBUILD_BUILDHOST
        unset ZBUILD_TOOLCHAIN
    else
        export ZBUILD_CROSS=1
        if [ ! -d $ZBUILD_TOOLCHAIN ];then
            echo "ERROR: ZBUILD_TOOLCHAIN($ZBUILD_TOOLCHAIN) not exist!"
            return 1
        fi
        if [ -n $ZBUILD_SYSROOT ]; then
            if [ ! -d $ZBUILD_SYSROOT/usr/lib ];then
                echo "ERROR: ZBUILD_SYSROOT($ZBUILD_SYSROOT) is empty or not exist!"
                echo "PLEASE CHECK SYSROOT: $ZBUILD_SYSROOT"
                return 1
            fi
        fi
    fi

    echo "------------------------------"
    if [ $# -ge 2 ];then
        release=$2
    else
        echo "build release version(default:no)? (no/yes)"
        read release
    fi
    if [ x"$release" = x"yes" ];then
        echo "Building version: release"
        export ZBUILD_RELEASE=1
    else
        echo "Building version: debug"
        export ZBUILD_RELEASE=0
    fi

    export ZBUILD_ENABLE=1
    if [ $# -ge 3 ];then
        if [ $3 -eq 0 ]; then
            export ZBUILD_ENABLE=0
        fi
    fi
    echo "Loading Config  : \"${config[selection]}\""
    echo "ZBUILD_PLATFORM : $ZBUILD_PLATFORM"
    echo "ZBUILD_PROJECT  : $ZBUILD_PROJECT"
    echo "ZBUILD_ARCH     : $ZBUILD_ARCH"
    echo "ZBUILD_SYSROOT  : $ZBUILD_SYSROOT"
    echo "ZBUILD_TOOLCHAIN: $ZBUILD_TOOLCHAIN"
}

function zbuild_clean()
{
    export WS_ROOT=
    export WS_OUTPUT=
    export ZBUILD_ENABLE=
    export ZBUILD_CROSS=
    export ZBUILD_RELEASE=
}

# 创建zbuild包
function zbuild_create_pkg()
{
    USAGE_INFO="[usage] zbuild_create_pkg <target_dir> <target_name>"
    if [ $# -ne 2 ];then
        echo $USAGE_INFO
        return
    else
        TARGET_DIR=$1
        TARGET_NAME=$2
    fi

    # 工程已经存在,则返回
    if [ -f "$TARGET_DIR/CMakeLists.txt" ];then
        echo "ERROR: package in $TARGET_DIR allready exist!"
        return
    fi

    # 目录不存在则创建
    if [ ! -d $TARGET_DIR ];then
        mkdir -p $TARGET_DIR
        mkdir -p $TARGET_DIR/inc
        mkdir -p $TARGET_DIR/src
    fi
    if [ ! -d $TARGET_DIR/inc ];then
        mkdir -p $TARGET_DIR/inc
    fi
    if [ ! -d $TARGET_DIR/src ];then
        mkdir -p $TARGET_DIR/src
    fi
    if [ ! -f "$TARGET_DIR/src/main.cpp" ];then
        cp $ZBUILD_ENVIRON/zbuild/template/zeros_main.cpptmp $TARGET_DIR/src/main.cpp
        if [ ! -f $TARGET_DIR/src/main.cpp ];then
            echo "ERROR: copty template file error!"
            return
        fi
        LINE_NUM=`sed -n '/zeros::init/=' $TARGET_DIR/src/main.cpp`
        if [ -n $LINE_NUM ];then
            sed -i "$LINE_NUM""c\    zeros::init(argc, argv, \"$TARGET_NAME\");" $TARGET_DIR/src/main.cpp
        fi
    fi

    # 拷贝工程模板
    cp $ZBUILD_ENVIRON/zbuild/template/CMakeLists.txt.in $TARGET_DIR/CMakeLists.txt
    # 编辑模板
    LINE_NUM=`sed -n '/# project()/=' $TARGET_DIR/CMakeLists.txt`
    if [ -n $LINE_NUM ];then
        sed -i "$LINE_NUM""c\project($TARGET_NAME)" $TARGET_DIR/CMakeLists.txt
    fi

    echo "zbuild create pakcage in $TARGET_NAME $TARGET_DIR ok."
}