#!/bin/sh
THIS_PATH=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
export WS_ROOT=`realpath $THIS_PATH/..`
export ZBUILD_ENABLE=0
ZBUILD_ENVIRON=$THIS_PATH
ZBUILD_PLATFORM=linux-amd64
ZBUILD_PROJECT=host
ZBUILD_ARCH=x86_64
ZBUILD_CROSS=0
ZBUILD_SYSROOT=
ZBUILD_TOOLCHAIN=

#检查环境是否正确
if [ -f "$ZBUILD_ENVIRON/zbuild/zbuild_cmd.sh" -a -f "$ZBUILD_ENVIRON/zbuild/template/CMakeLists.txt.in" -a -d "$ZBUILD_ENVIRON/config" ]; then
echo -e "\033[33m\033[1m"
cat <<EOF
-------------------------------------------------------------------------------
| [ environment setup ] <20210712-v1.0>                                       |
| --------------------------------------                                      |
| Invoke "source ./environ/envsetup.sh" from your shell to setup the building |
| environment, then you can build this project in ROS or non-ROS environment. |
|                                                                             |
| In ROS environment, you can use catkin or colcon tools as you please. Before|
| build, you must set three variables:                                        |
| - WS_ROOT: the workspace root path.                                         |
| - WS_OUTPUT: the build output path.                                         |
| - WS_ROSBUILD: the ros build tools("catkin" or "concol").                   |
|                                                                             |
| In non-ROS environment, you should use the zbuild tools as below:           |
| - zbuild_setup: use zbuild and setup your workspace.                        |
| - zbuild_clean: cleanup zbuild when you do not want it.                     |
| - zbuild_create_pkg: create a zbuild package.                               |
-------------------------------------------------------------------------------
EOF
echo -e "\033[0m"
else
    echo -e '\033[33m\033[1mYou should invoke ". environ/envsetup.sh" in project top directory!'
    echo -e "come back to $HOME\033[0m"
    exit 1
fi

source $ZBUILD_ENVIRON/zbuild/zbuild_cmd.sh