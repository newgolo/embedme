set(REQUIRED_CMAKE_VERSION 3.0)
cmake_minimum_required(VERSION ${REQUIRED_CMAKE_VERSION})

if(NOT WIN32)
  string(ASCII 27 Esc)
  set(Red         "${Esc}[31m")
  set(Green       "${Esc}[32m")
  set(Yellow      "${Esc}[33m")
  set(ColorEnd  "${Esc}[m")

function(log_info msg)
    message("${Green}${msg}${ColorEnd}")
endfunction()

function(log_warn msg)
    message("${Yellow}${msg}${ColorEnd}")
endfunction()

function(log_error msg)
    message(FATAL_ERROR "${Red}${msg}${ColorEnd}")
endfunction()

else()
function(log_info msg)
    message(STATUS "${msg}")
endfunction()

function(log_warn msg)
    message(STATUS "${msg}")
endfunction()

function(log_error msg)
    message(FATAL_ERROR "${msg}")
endfunction()

endif()

# 打印cmake环境变量
function(printenv_cmake project)
    log_warn("-----------------------CMAKE ENV---------------------------")
    message("${Yellow}PROJECT:${ColorEnd} ${project}")
    message("${Yellow}CMAKE_SYSROOT:${ColorEnd} ${CMAKE_SYSROOT}")
    message("${Yellow}CMAKE_SYSROOT_COMPILE:${ColorEnd} ${CMAKE_SYSROOT_COMPILE}")
    message("${Yellow}CMAKE_SYSTEM_PROCESSOR:${ColorEnd} ${CMAKE_SYSTEM_PROCESSOR}")
    message("${Yellow}CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES:${ColorEnd} ${CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES}")
    message("${Yellow}CMAKE_CXX_IMPLICIT_LINK_DIRECTORIES:${ColorEnd} ${CMAKE_CXX_IMPLICIT_LINK_DIRECTORIES}")
    message("${Yellow}CMAKE_SYSTEM_PREFIX_PATH:${ColorEnd} ${CMAKE_SYSTEM_PREFIX_PATH}")
    message("${Yellow}CMAKE_PREFIX_PATH:${ColorEnd} ${CMAKE_PREFIX_PATH}")
    message("${Yellow}CMAKE_MODULE_PATH:${ColorEnd} ${CMAKE_MODULE_PATH}")
    message("${Yellow}CMAKE_LIBRARY_PATH:${ColorEnd} ${CMAKE_LIBRARY_PATH}")
    message("${Yellow}WS_ROOT:${ColorEnd} ${WS_ROOT}")
    message("${Yellow}WS_OUTPUT:${ColorEnd} ${WS_OUTPUT}")
    message("${Yellow}WS_ROSBUILD:${ColorEnd} ${WS_ROSBUILD}")
    message("${Yellow}ZBUILD_ENABLE:${ColorEnd} ${ZBUILD_ENABLE}")
    # message(${Yellow}"CMAKE_FRAMEWORK_PATH:${ColorEnd} ${CMAKE_FRAMEWORK_PATH}")
    # message(${Yellow}"CMAKE_APPBUNDLE_PATH:${ColorEnd} ${CMAKE_APPBUNDLE_PATH}")
    log_warn("-----------------------------------------------------------")
endfunction()

# cpplint代码检查
function(add_cpplint src_abs_path)
    find_program(CPP_LINT cpplint)
    string(JOIN "," CPPLINT_FILTERS
        "-build/header_guard"
        "-build/c++11"
        "-legal/copyright"
        "-whitespace/indent"
        "+whitespace/comment"
    )
    string(REPLACE "/" "_" CPPLINT_TARGET ${src_abs_path})
    file(GLOB_RECURSE ALL_SOURCE_FILES 
        ${src_abs_path}/*.cpp 
        ${src_abs_path}/*.cc 
        ${src_abs_path}/*.c
        ${src_abs_path}/*.hpp 
        ${src_abs_path}/*.h 
    )
    add_custom_target(cpplint_${CPPLINT_TARGET} ALL
        COMMAND ${CPP_LINT} --quiet --filter=${CPPLINT_FILTERS}
        ${ALL_SOURCE_FILES}
    )
endfunction()