## zbuild_add_submodules(${CMAKE_CURRENT_SOURCE_DIR})
## 增加模块子目录,参数current_dir必须是绝对路径
function(zbuild_add_submodules current_dir)
    file(GLOB children RELATIVE ${current_dir} ${current_dir}/*)
    foreach(child ${children})
        if(IS_DIRECTORY ${current_dir}/${child})
            LIST(APPEND dir_list ${child})
        endif()
    endforeach()
    foreach(module_dir ${dir_list})
        if(EXISTS "${current_dir}/${module_dir}/CMakeLists.txt")
            add_subdirectory(${module_dir})
            # message("modules add: ${module_dir}")
        endif()
    endforeach()
endfunction()

## 安装模块bin到preinstall目录
function(zbuild_install_bin target)
    if (NOT DEFINED PREINSTALL_DIR)
        message(FATAL_ERROR "not define PREINSTALL_DIR")
    endif()
    install(TARGETS ${target} 
        DESTINATION ${PREINSTALL_DIR}/bin/
    )
    if (NOT ${ZBUILD_STRIP} STREQUAL "")
        add_custom_command(
            TARGET ${target}
            POST_BUILD
            COMMAND ${ZBUILD_STRIP} ${target}
        )
    endif()
endfunction()

## 安装模块lib到preinstall目录
function(zbuild_install_lib target)
    if (NOT DEFINED PREINSTALL_DIR)
        message(FATAL_ERROR "not define PREINSTALL_DIR")
    endif()
    install(TARGETS ${target} 
        DESTINATION ${PREINSTALL_DIR}/lib/
    )
    if (NOT ${ZBUILD_STRIP} STREQUAL "")
        if ($ENV{ZBUILD_SYSNAME} STREQUAL "Windows")
            set(target_name lib${target}.dll)
        else()
            set(target_name lib${target}.so)
        endif()
        add_custom_command(
            TARGET ${target}
            POST_BUILD
            COMMAND ${ZBUILD_STRIP} ${target_name}
        )
    endif()
endfunction()

## 安装模块launch文件到preinstall目录
function(zbuild_install_launch launch_file node_name)
    install(FILES ${launch_file}
        DESTINATION ${PREINSTALL_DIR}/share/launch/${node_name}/
    )
endfunction()

## 安装模块share文件到preinstall目录
function(zbuild_install_share type target dest_dir)
    if (NOT DEFINED PREINSTALL_DIR)
        message(FATAL_ERROR "not define PREINSTALL_DIR")
    endif()
    if(${type} STREQUAL "DIRECTORY")
        install(DIRECTORY ${target}
            DESTINATION ${PREINSTALL_DIR}/share/${dest_dir}
            USE_SOURCE_PERMISSIONS
        )
    elseif(${type} STREQUAL "FILES")
        install(FILES ${target}
            DESTINATION ${PREINSTALL_DIR}/share/${dest_dir}
        )
    else()
        message(FATAL_ERROR 
            "error: zbuild_install_share [DIRECTORY | FILES] target dest_dir"
        )
    endif()
endfunction()