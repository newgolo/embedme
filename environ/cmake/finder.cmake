# 查找Boost库
function(find_Boost MANUALLY)
    set(Boost_INCS ${SYSROOT_PATH}/usr/include PARENT_SCOPE)
    set(Boost_LIBS
        boost_container
        boost_serialization
        boost_iostreams
        boost_coroutine
        boost_system
        boost_thread
        PARENT_SCOPE)
    if(NOT MANUALLY)
        find_package(Boost REQUIRED)
    endif()
    if(${Boost_FOUND})
        if(NOT "${Boost_INCLUDE_DIRS}" STREQUAL "")
            set(Boost_INCS ${Boost_INCLUDE_DIRS} PARENT_SCOPE)
        endif()
        if(NOT "${Boost_LIBRARIES}" STREQUAL "")
            set(Boost_LIBS ${Boost_LIBRARIES} PARENT_SCOPE)
        endif()
    endif()
endfunction(find_Boost)

# 查找OpenCV库
function(find_OpenCV MANUALLY)
    set(OpenCV_INCS ${SYSROOT_PATH}/usr/include PARENT_SCOPE)
    set(OpenCV_LIBS 
        opencv_calib3d
        opencv_highgui
        opencv_imgcodecs
        opencv_imgproc
        opencv_videoio
        opencv_core
        opencv_features2d
        PARENT_SCOPE)
    if(NOT MANUALLY)
        find_package(OpenCV REQUIRED)
    endif()
    if(${OpenCV_FOUND})
        if(NOT "${OpenCV_INCLUDE_DIRS}" STREQUAL "")
            set(OpenCV_INCS ${OpenCV_INCLUDE_DIRS} PARENT_SCOPE)
        endif()

        if(NOT "${OpenCV_LIBRARIES}" STREQUAL "")
            set(OpenCV_LIBS ${OpenCV_LIBRARIES} PARENT_SCOPE)
        endif()
    endif()
endfunction(find_OpenCV)

function(find_PCL MANUALLY)
    set(PCL_INCS ${SYSROOT_PATH}/usr/include PARENT_SCOPE)
    set(PCL_LIBS
        pcl_io
        PARENT_SCOPE
    )
    if(NOT MANUALLY)
        find_package(PCL REQUIRED)
    endif()
    if(${PCL_FOUND})
            if(NOT "${PCL_INCLUDE_DIRS}" STREQUAL "")
                set(PCL_INCS ${PCL_INCLUDE_DIRS} PARENT_SCOPE)
            endif()

            if(NOT "${PCL_LIBRARIES}" STREQUAL "")
                set(PCL_LIBS ${PCL_LIBRARIES} PARENT_SCOPE)
            endif()
    endif()
endfunction(find_PCL)
