#!/bin/bash
THIS_PATH=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
###########################################################
# 设置容器参数
PROJECT_NAME=appkit
CONTAINER=build-$PROJECT_NAME
DOCKERIMG=noetic-aarch64-crossenv-img:latest
###########################################################
if [ "$1" == "build" ];then
    docker pull ${DOCKERIMG}
    docker stop $CONTAINER
    docker rm $CONTAINER
    docker run -it $USER_ARGS --name $CONTAINER --entrypoint="/root/$PROJECT_NAME/docker_crossenv_build.sh" -v $THIS_PATH:/root/$PROJECT_NAME $DOCKERIMG bash 
elif [ "$1" == "zbuild" ]; then
    docker pull ${DOCKERIMG}
    docker stop $CONTAINER
    docker rm $CONTAINER
    docker run -it $USER_ARGS --name $CONTAINER --entrypoint="/root/$PROJECT_NAME/docker_crossenv_zbuild.sh" -v $THIS_PATH:/root/$PROJECT_NAME $DOCKERIMG bash 
elif [ "$1" == "create" ]; then
    docker run -it $USER_ARGS --name $CONTAINER --network=host --entrypoint="" -v $THIS_PATH:/root/$PROJECT_NAME $DOCKERIMG bash 
elif [ "$1" == "start" ]; then
    docker start $CONTAINER         # 启动容器
    docker attach $CONTAINER        # 进入容器
elif [ "$1" == "run" ]; then        # 执行一个已经开启的容器
    docker start $CONTAINER
    docker exec -it $CONTAINER bash
else
    echo "$0 [build | create | start | run]"
fi
