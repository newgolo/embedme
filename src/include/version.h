// This file was create automatically by CMake.
#ifndef APPLICATION_VERSION_H
#define APPLICATION_VERSION_H

// Whether or not we retrieved the state of the repo.
#define GIT_RETRIEVED_STATE true

// The SHA1 for the HEAD of the repo.
#define GIT_HEAD_SHA1 "ff523598c1cebab8a50fa75db2b1e84f207ac437"

// Whether or not there were uncommited changes present.
#define GIT_IS_DIRTY false

// The name of person who committed HEAD.
#define GIT_AUTHOR_NAME "newgolo"

// The email addess of the person who committed HEAD.
#define GIT_AUTHOR_EMAIL "cblock@126.com"

// When HEAD was committed.
#define GIT_COMMIT_DATE_ISO8601 "2023-12-29 18:20:25 +0800"

// The subject line for the HEAD commit.
#define GIT_COMMIT_SUBJECT "修改README"

// The notes attached to the HEAD commit.
#define GIT_COMMIT_BODY ""

// The output from git --describe (e.g. the most recent tag)
#define GIT_DESCRIBE "V2.2.0-37-gff523598"

#define GIT_TAG "V2.2.0-37-gff523598"

#endif
