#pragma once

namespace common {
namespace file_path {
static const char* kBaseControlConfig =
    "share/base_control/config/base_control.yaml";
}
}  // namespace common
