###############################################################################
# 每个Package目录下的CMakeLists.txt都要include本文件，用于初始化Package：
# 例如：src/appkit/CMakeLists.txt
# include(${CMAKE_CURRENT_SOURCE_DIR}/../setup.cmake)
###############################################################################
# 编译前需配置工作空间环境变量：
# export WS_ROOT=$PWD
# export WS_OUTPUT=$PWD/output

###############################################################################
# 设置工程根目录
set(WS_ROOT $ENV{WS_ROOT})
if (WS_ROOT)
    message("==>Workspace Path: ${WS_ROOT}")
else()
    message(FATAL_ERROR "environment variable WS_ROOT not set!")
endif()

# 设置protobuf安装路径
set(PROTOC_INSTALL_PATH $ENV{PROTOC_INSTALL_PATH})
if (NOT PROTOC_INSTALL_PATH)
    set(PROTOC_INSTALL_PATH /usr/bin)
endif()
message("==>Protoc Path: ${PROTOC_INSTALL_PATH}")

# 设置当前环境ROS发行版名称
set(ROS_DISTRO $ENV{ROS_DISTRO})

# 检测是否使用zbuild编译(非ROS环境)
set(ZBUILD_ENABLE $ENV{ZBUILD_ENABLE})
set(ZBUILD_CROSS $ENV{ZBUILD_CROSS})
set(WS_OUTPUT $ENV{WS_OUTPUT})
if(${ZBUILD_ENABLE})
    if(WS_OUTPUT)
        if(NOT EXISTS ${WS_OUTPUT})
            file(MAKE_DIRECTORY ${WS_OUTPUT})
        endif()
    elseif($ENV{WS_OUTPUT}x STREQUAL "x")
        set(WS_OUTPUT ${WS_ROOT})
    endif()
else()
    set(WS_OUTPUT $ENV{WS_OUTPUT})
    if(WS_OUTPUT)
        if(NOT EXISTS ${WS_OUTPUT})
            file(MAKE_DIRECTORY ${WS_OUTPUT})
        endif()
    else()
        set(WS_OUTPUT ${WS_ROOT})
    endif()
endif()
message("==>Output Path   : ${WS_OUTPUT}")

set(WS_ROSBUILD $ENV{WS_ROSBUILD})
include(${WS_ROOT}/environ/cmake/common.cmake)
include(${WS_ROOT}/environ/cmake/finder.cmake)

###############################################################################
# cmake配置
# cmake_policy(SET CMP0022 OLD)             # 设置cmake策略
# set(CMAKE_VERBOSE_MAKEFILE ON)            # 打开makefile调试信息
set(USE_SHARED_LIB 1)                       # 默认编译动态库
set(CMAKE_BUILD_TYPE "Release")             # Debug,Release,RelWithDebInfo,MinSizeRel
set(CMAKE_CXX_STANDARD 14)                  # C++版本
set(CMAKE_POSITION_INDEPENDENT_CODE ON)     # -fPIC
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fopenmp")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fopenmp")
SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fopenmp")
set(CMAKE_PREFIX_PATH ${WS_OUTPUT}/install)
set(CMAKE_MODULE_PATH "${CMAKE_PREFIX_PATH}/share/cmake")

# 编译选项配置
# add_compile_options(-g)
# add_compile_options(-Wall)
add_compile_options("-Wl,--copy-dt-needed-entries") # avoid error:"DSO missing from command line"
add_compile_options("-Wl,--gc-sections")

if (USE_SHARED_LIB)
    set(LIB_BUILD_TYPE SHARED)
else()
    set(LIB_BUILD_TYPE STATIC)
endif()

# 设置安装目录
set(INSTALL_PATH ${WS_OUTPUT}/install)
set(INSTALL_BIN_PATH ${INSTALL_PATH}/bin)
set(INSTALL_LIB_PATH ${INSTALL_PATH}/lib)
set(INSTALL_INC_PATH ${INSTALL_PATH}/include)
set(INSTALL_SHARE_PATH ${INSTALL_PATH}/share)

# 公共头文件
include_directories("${WS_ROOT}/src/include")

# 系统链接库
set(LIB_SYSTEM pthread rt dl)

###############################################################################
# 功能模块控制
set(USE_SYSV 1) #是否使用sysv
set(USE_UUID 0) #是否使用uuid

if(${USE_UUID})
set(LIB_UUID uuid)
endif()

# 定义宏定义
add_definitions(-DUSE_SYSV=${USE_SYSV})
add_definitions(-DUSE_UUID=${USE_UUID})

# 导入要参与编译的模块
include(${WS_ROOT}/prebuilt/package.cmake)
set(PKG_THIRD_PARTY_PATH ${WS_ROOT}/src/third_party)
include(${PKG_THIRD_PARTY_PATH}/package.cmake)
include(${WS_ROOT}/src/appkit/package.cmake)

# 设置ROS环境下的搜索目录
if(NOT(${ZBUILD_ENABLE}))
    if (${WS_ROSBUILD} MATCHES "colcon")
        link_directories(${WS_OUTPUT}/build/third_party/devel/lib)
    endif()
    include_directories(${CMAKE_PREFIX_PATH}/include)
    link_directories(${CMAKE_PREFIX_PATH}/lib)
    include_directories(${WS_OUTPUT}/devel/include)
    link_directories(${WS_OUTPUT}/devel/lib)
    list(APPEND CMAKE_PREFIX_PATH /opt/ros/${ROS_DISTRO})
    list(APPEND CMAKE_MODULE_PATH /opt/ros/${ROS_DISTRO}/share/catkin/cmake)
else()
    if(NOT EXISTS ${INSTALL_BIN_PATH})
        file(MAKE_DIRECTORY ${INSTALL_BIN_PATH})
    endif()
    if(NOT EXISTS ${INSTALL_LIB_PATH})
        file(MAKE_DIRECTORY ${INSTALL_LIB_PATH})
    endif()
    if(NOT EXISTS ${INSTALL_SHARE_PATH})
        file(MAKE_DIRECTORY ${INSTALL_SHARE_PATH})
    endif()
endif()
