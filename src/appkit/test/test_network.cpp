#include <appkit/async.h>
#include <appkit/console.h>
#include <appkit/datetime.h>
#include <appkit/network.h>
#include <appkit/strutil.h>
#include <appkit/tracer.h>

using namespace appkit;  // NOLINT

void test_network() {
    while (1) {
        TRACE_YELLOW("----------network test--------------");
        TRACE_YELLOW("01. get mac address.");
        TRACE_YELLOW(" q. quit");
        TRACE_YELLOW("please input selection:");
        ConsoleIn ci;
        ci.waitInput();
        if (ci.asString() == "01") {
            auto mac = NetUtil::getMacAddr("eth0");
            if (mac.size() != 6) {
                TRACE_ERR("mac size error: %s!", mac.size());
            }

            TRACE_CYAN("MAC %02x:%02x:%02x:%02x:%02x:%02x", mac[0], mac[1],
                      mac[2], mac[3], mac[4], mac[5]);
        } else if (ci.asString() == "02") {
            //
        } else if (ci.asString() == "q" || ci.asString() == "quit") {
            break;
        }
    }
}