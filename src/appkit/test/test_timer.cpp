#include <appkit/async.h>
#include <appkit/console.h>
#include <appkit/datetime.h>
#include <appkit/strutil.h>
#include <appkit/timer.h>
#include <appkit/tracer.h>

using namespace appkit;

void test_timer_manager() {
    Timer t1(500, true);
    Timer t2(1000, true);
    TimerManager tm;
    tm.registerTimer(t1, []() { TRACE_INFO("500 ms timer timeout!"); });
    tm.registerTimer(t2, []() { TRACE_INFO("1000 ms timer timeout!"); });

    while (1) {
        TRACE_YELLOW("01. start.");
        TRACE_YELLOW("02. stop.");
        TRACE_YELLOW("input q to quit!");
        ConsoleIn ci;
        ci.waitInput();
        if (ci.asString() == "01") {
            tm.start();
        } else if (ci.asString() == "02") {
            tm.stop();
        } else if (ci.asString() == "q" || ci.asString() == "quit") {
            break;
        }
    }
    tm.stop();
}

void test_timer() {
    while (1) {
        TRACE_YELLOW("----------timer test--------------");
        TRACE_YELLOW("01. timer manager test.");
        TRACE_YELLOW(" q. quit");
        TRACE_YELLOW("please input selection:");
        ConsoleIn ci;
        ci.waitInput();
        if (ci.asString() == "01") {
            test_timer_manager();
        } else if (ci.asString() == "02") {
            //
        } else if (ci.asString() == "q" || ci.asString() == "quit") {
            break;
        }
    }
}