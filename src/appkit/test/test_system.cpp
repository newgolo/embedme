#include <appkit/async.h>
#include <appkit/console.h>
#include <appkit/datetime.h>
#include <appkit/strutil.h>
#include <appkit/system.h>
#include <appkit/timer.h>
#include <appkit/tracer.h>

using namespace appkit;

void test_system() {
    while (1) {
        TRACE_YELLOW("----------system test--------------");
        TRACE_YELLOW("01. execute test.");
        TRACE_YELLOW(" q. quit");
        TRACE_YELLOW("please input selection:");
        ConsoleIn ci;
        ci.waitInput();
        if (ci.asString() == "01") {
            std::string cmd = "top -d 1";
            std::string result;
            if(RC_OK!=System::execute(cmd, &result, 30000)){
                TRACE_ERR("execute [%s] error!",cmd.data());
            } else {
                TRACE_ERR("execute [%s] --> [%s]",cmd.data(), result.data());
            }
        } else if (ci.asString() == "02") {
            //
        } else if (ci.asString() == "q" || ci.asString() == "quit") {
            break;
        }
    }
}