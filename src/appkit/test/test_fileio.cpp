#include <appkit/async.h>
#include <appkit/console.h>
#include <appkit/datetime.h>
#include <appkit/fileio.h>
#include <appkit/strutil.h>
#include <appkit/timer.h>
#include <appkit/tracer.h>

using namespace appkit;  // NOLINT

void test_fileio() {
    while (1) {
        TRACE_YELLOW("----------fileio test--------------");
        TRACE_YELLOW("01. getFileList() test.");
        TRACE_YELLOW("02. getFileList(...) test.");
        TRACE_YELLOW(" q. quit");
        TRACE_YELLOW("please input selection:");
        ConsoleIn ci;
        ci.waitInput();
        if (ci.asString() == "01") {
            Directory dir;
            dir.enterDir("/var/log");
            auto fileList = dir.getFileList();
            for (auto file : fileList) {
                TRACE_INFO("file: %s", file.data());
            }
        } else if (ci.asString() == "02") {
            auto fileList = Directory::getFileList(
                "/home/fergus/test", "^([a-zA-Z0-9]+)([a-zA-Z0-9_]*)\\.yaml$");
            TRACE_INFO("get file list: %d", fileList.size());
            for (auto& filePath : fileList) {
                TRACE_INFO("file: %s", filePath.data());
            }

        } else if (ci.asString() == "q" || ci.asString() == "quit") {
            break;
        }
    }
}