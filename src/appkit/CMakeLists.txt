include(${CMAKE_CURRENT_SOURCE_DIR}/../setup.cmake)
cmake_minimum_required(VERSION ${REQUIRED_CMAKE_VERSION})
project(appkit)
printenv_cmake(${PROJECT_NAME})

if(NOT(${ZBUILD_ENABLE}))
    find_package(catkin REQUIRED)
    catkin_package(
        # INCLUDE_DIRS inc
        LIBRARIES appkit
        #  CATKIN_DEPENDS other_catkin_pkg
        #  DEPENDS system_lib
    )
    # 调用catkin_package后再使用CATKIN_PACKAGE_xxx变量
    set(INSTALL_BIN_PATH ${CATKIN_PACKAGE_BIN_DESTINATION})
    set(INSTALL_LIB_PATH ${CATKIN_PACKAGE_LIB_DESTINATION})
    set(INSTALL_INC_PATH ${CATKIN_PACKAGE_INCLUDE_DESTINATION})
    set(INSTALL_SHARE_PATH ${CATKIN_PACKAGE_SHARE_DESTINATION})
endif()

include_directories(
    inc
    ${INSTALL_INC_PATH}
)

set(PRE_CONFIGURE_FILE "${CMAKE_CURRENT_SOURCE_DIR}/../include/version.h.in")
set(POST_CONFIGURE_FILE "${CMAKE_CURRENT_SOURCE_DIR}/../include/version.h")
include(${CMAKE_CURRENT_SOURCE_DIR}/../../environ/cmake/git_watcher.cmake)

add_subdirectory(src)
add_subdirectory(test)

if(NOT(${ZBUILD_ENABLE}))
    install(DIRECTORY inc/${PROJECT_NAME}/
        DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
        FILES_MATCHING PATTERN "*.h"
        PATTERN ".svn" EXCLUDE
    )
endif()
