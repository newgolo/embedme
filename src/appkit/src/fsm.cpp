/******************************************************************************
This file is part of AppKit.
Project: appkit
Author : FergusZeng
Email  : cblock@126.com
git	   : https://gitee.com/newgolo/appkit.git
*******************************************************************************
MIT License

Copyright (c) 2022 cblock@126.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
******************************************************************************/
#include "appkit/fsm.h"
#include "appkit/tracer.h"

namespace appkit {
FSMachine::FSMachine() {}

FSMachine::~FSMachine() {
    m_stateHandlerMap.clear();
    m_stateListenerSet.clear();
}

bool FSMachine::initState(int state) {
    if (state < 0) {
        return false;
    }
    m_currState = state;
    return true;
}

bool FSMachine::addState(int state,
                              std::function<int(void)> stateHandler) {
    if (state < 0) {
        TRACE_ERR_CLASS("state cannot be negative,state(%d)!", state);
        return false;
    }
    auto iter = m_stateHandlerMap.find(state);
    if (iter != m_stateHandlerMap.end()) {
        return false;
    }
    m_stateHandlerMap.insert({state, stateHandler});
    return true;
}

void FSMachine::addListener(std::shared_ptr<FSListener> listener) {
    m_stateListenerSet.insert({listener});
}

int FSMachine::getState() { return m_currState; }

void FSMachine::processState() {
    if (!m_stateHandlerMap.empty()) {
        auto iter = m_stateHandlerMap.find(m_currState);
        int tmp = m_currState;
        if (iter != m_stateHandlerMap.end()) {
            m_currState = iter->second();
            for (auto& listener : m_stateListenerSet) {
                listener->onStateChanged(m_currState);
            }
        }
    }
}
}  // namespace appkit
