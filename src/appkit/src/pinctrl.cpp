/******************************************************************************
This file is part of AppKit.
Project: appkit
Author : FergusZeng
Email  : cblock@126.com
git	   : https://gitee.com/newgolo/appkit.git
*******************************************************************************
MIT License

Copyright (c) 2022 cblock@126.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
******************************************************************************/
#include "appkit/pinctrl.h"

#include <string.h>

#include "appkit/fileio.h"
#include "appkit/strutil.h"
#include "appkit/tracer.h"
namespace appkit {

PinCtrl::PinCtrl(const std::string& group, int pin, const std::string& base)
    : m_group(group), m_pin(pin), m_groupBase(base) {}

PinCtrl::~PinCtrl() {
    if (m_pinID >= 0) {
        File unexportFile;
        if (!unexportFile.open(m_unexportFile, IO_MODE_WR_ONLY)) {
            TRACE_ERR_CLASS("cannot export, pin [%s:%d] !", CSTR(m_group),
                            m_pin);
            return;
        }
        std::string data = StrUtil::format("%d", m_pinID);
        if (unexportFile.writeData(data.data(), data.size()) != data.size()) {
            TRACE_ERR_CLASS("export error, pin [%s:%d] !", CSTR(m_group),
                            m_pin);
        }
    }
    m_pinID = -1;
}

bool PinCtrl::isRequested() { return (m_pinID < 0) ? false : true; }

bool PinCtrl::request(const PinDir& dir) {
    m_pinID = getPinID(m_group, m_pin);
    if (m_pinID < 0) {
        TRACE_ERR_CLASS("pin[%s:%d] getID error!", CSTR(m_group), m_pin);
        return false;
    }
    m_exportFile = "/sys/class/gpio/export";
    m_unexportFile = "/sys/class/gpio/unexport";
    m_gpioDir = "/sys/class/gpio/gpio";
    m_gpioDir += StrUtil::format("%d", m_pinID);
    m_directionFile = m_gpioDir + "/direction";
    m_valueFile = m_gpioDir + "/value";

    /* 导出gpio */
    if (!Directory::exists(m_gpioDir)) {
        File exportFile;
        if (!exportFile.open(m_exportFile, IO_MODE_WR_ONLY)) {
            TRACE_ERR_CLASS("cannot open export file, pin [%s:%d] ",
                            CSTR(m_group), m_pin);
            return false;
        }
        std::string data = StrUtil::format("%d", m_pinID);
        if (exportFile.writeData(data.data(), data.size()) != data.size()) {
            TRACE_ERR_CLASS("cannot export, pin [%s:%d] !", CSTR(m_group),
                            m_pin);
            return false;
        }
    }

    /* 判断是否已导出 */
    if (File::exists(m_directionFile) && File::exists(m_valueFile)) {
        File directionFile;
        if (!directionFile.open(m_directionFile, IO_MODE_WR_ONLY)) {
            TRACE_ERR_CLASS("cannot open direction file, pin [%s:%d] ",
                            CSTR(m_group), m_pin);
            return false;
        }
        std::string data;
        switch (dir) {
            case PinDir::IN:
                data = "in";
                break;
            case PinDir::OUT:
                data = "out";
                break;
            default:
                m_pinID = -1;
                return false;
        }
        if (directionFile.writeData(data.data(), data.size()) != data.size()) {
            TRACE_ERR_CLASS("cannot set direction, pin [%s:%d]", CSTR(m_group),
                            m_pin);
            return false;
        }
    } else {
        TRACE_ERR_CLASS("export error, pin [%s:%d]", CSTR(m_group), m_pin);
        return false;
    }
    return true;
}

bool PinCtrl::set(const PinVal& val) {
    if (m_pinID < 0) {
        TRACE_ERR_CLASS("pin[%s:%d] not request!", CSTR(m_group), m_pin);
        return false;
    }
    File valueFile;
    if (!valueFile.open(m_valueFile, IO_MODE_WR_ONLY)) {
        TRACE_ERR_CLASS("cannot open value file, pin [%s:%d]", CSTR(m_group),
                        m_pin);
        return false;
    }
    std::string data;
    switch (val) {
        case PinVal::LOW:
            data = "0";
            break;
        case PinVal::HIGH:
            data = "1";
            break;
        default:
            TRACE_ERR_CLASS("invalid param, pin [%s:%d]", CSTR(m_group), m_pin);
            return false;
    }
    if (valueFile.writeData(data.data(), data.size()) != data.size()) {
        TRACE_ERR_CLASS("cannot set value, pin [%s:%d]", CSTR(m_group), m_pin);
        return false;
    }
    return true;
}

bool PinCtrl::get(PinVal* val) {
    if (m_pinID < 0) {
        TRACE_ERR_CLASS("pin[%s:%d] not request!", CSTR(m_group), m_pin);
        return false;
    }
    File valueFile;
    if (!valueFile.open(m_valueFile, IO_MODE_RDWR_ONLY)) {
        TRACE_ERR_CLASS("cannot open value file, pin [%s:%d]", CSTR(m_group),
                        m_pin);
        return false;
    }

    char data;
    if (valueFile.readData(&data, 1) != 1) {
        return false;
    }
    switch (data) {
        case '0':
            *val = PinVal::LOW;
            break;
        case '1':
            *val = PinVal::HIGH;
            break;
        default:
            TRACE_ERR_CLASS("invalid value: %c, pin [%s:%d]", data,
                            CSTR(m_group), m_pin);
            return false;
    }
    return true;
}

std::string PinCtrl::pinPath() {
    if (m_pinID < 0) {
        TRACE_ERR_CLASS("pin[%s:%d] not request!", CSTR(m_group), m_pin);
        return "";
    }
    return m_valueFile;
}

/******************************************************************************************
 * PinGroup的label和base信息可在/sys/class/gpio/gpiochipxx中查看,每种平台可能都不一样.
 * 可通过shell查看:
 * $ cd /sys/class/gpio; for i in gpiochip*; do echo `cat $i/label`: `cat
 *$i/base`; done
 *****************************************************************************************/
int PinCtrl::getPinID(const std::string& group, int pin) {
    if (group.size() == m_groupBase.size()) {
        int grp = StrUtil::compare(group, m_groupBase);
        if (grp >= 0) {
            return 32 * grp + pin;
        }
    }
    return -1;
}

}  // namespace appkit
