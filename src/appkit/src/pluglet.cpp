/******************************************************************************
This file is part of AppKit.
Project: appkit
Author : FergusZeng
Email  : cblock@126.com
git	   : https://gitee.com/newgolo/appkit.git
*******************************************************************************
MIT License

Copyright (c) 2022 cblock@126.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
******************************************************************************/
#include "appkit/pluglet.h"

#include <dlfcn.h>

#include "appkit/fileio.h"
#include "appkit/tracer.h"

namespace appkit {
Pluglet::Pluglet() {}

Pluglet::~Pluglet() { close(); }

bool Pluglet::isOpen() { return !!m_plugHandle; }

bool Pluglet::open(std::string plugPath, PlugletType type) {
    int flags = -1;
    if (plugPath.empty() || !File::exists(plugPath)) {
        return false;
    }
    switch (type) {
        case PlugletType::Lazy:
            flags = RTLD_LAZY;
            break;
        case PlugletType::Now:
            flags = RTLD_NOW;
        default:
            return false;
    }
    m_plugHandle = dlopen(CSTR(plugPath), flags);
    if (!m_plugHandle) {
        TRACE_ERR_CLASS("open pluglet error:%s", dlerror());
        return false;
    }
    return true;
}

bool Pluglet::putSymbol(std::string symName) {
    if (!m_plugHandle) {
        return false;
    }
    void* sym = dlsym(m_plugHandle, CSTR(symName));
    if (!sym) {
        return false;
    }
    m_symbolMap.insert(std::make_pair(symName, sym));
    return true;
}

void* Pluglet::getSymbol(std::string symName) {
    auto iter = m_symbolMap.find(symName);
    if (iter != m_symbolMap.end()) {
        return iter->second;
    }
    return nullptr;
}

void Pluglet::close() {
    if (m_plugHandle) {
        dlclose(m_plugHandle);
        m_plugHandle = nullptr;
    }
}
}  // namespace appkit
