/******************************************************************************
This file is part of AppKit.
Project: appkit
Author : FergusZeng
Email  : cblock@126.com
git	   : https://gitee.com/newgolo/appkit.git
*******************************************************************************
MIT License

Copyright (c) 2022 cblock@126.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
******************************************************************************/
#include "appkit/regex.h"

#include <regex.h>
#include <stdlib.h>

#include "appkit/tracer.h"

namespace appkit {
Regex::Regex() {}

Regex::~Regex() {}

bool Regex::match(const std::string& pattern, const std::string& source) {
    std::string str;
    int pos;
    return match(pattern, source, &str, &pos);
}

bool Regex::match(const std::string& pattern, const std::string& source,
                  std::string* result, int* pos) {
    std::vector<std::string> strArray;
    std::vector<int> posArray;
    result->clear();
    bool rc = match(pattern, source, &strArray, &posArray, 1);
    if (rc) {
        *result = strArray[0];
        *pos = posArray[0];
    }
    return rc;
}

bool Regex::match(const std::string& pattern, const std::string& source,
                  std::vector<std::string>* strArray,
                  std::vector<int>* posArray, int maxMatches) {
    bool isMatched = false;
    regex_t regex;
    if (maxMatches <= 0) {
        maxMatches = 1;
    }
    if (0 != regcomp(&regex, pattern.data(), REG_EXTENDED)) {
        return false;
    }
    strArray->clear();
    posArray->clear();
    char* pStr = const_cast<char*>(source.data());
    char* pStart = pStr;
    for (auto i = 0; i < maxMatches; i++) {
        regmatch_t regmatch;
        int rc = regexec(&regex, pStr, 1, &regmatch, 0);
        if (rc == REG_NOMATCH) {
            break;
        } else if (rc < 0) {
            TRACE_ERR_CLASS("regexec error!");
            break;
        } else {
            isMatched = true;
            if (regmatch.rm_so != -1) {
                std::string result = std::string(pStr).substr(
                    regmatch.rm_so, regmatch.rm_eo - regmatch.rm_so);
                posArray->push_back(
                    static_cast<int>(pStr + regmatch.rm_so - pStart));
                strArray->push_back(result);
                pStr = pStr + regmatch.rm_eo;
            } else {
                break;
            }
        }
    }
    regfree(&regex);
    return isMatched;
}
}  // namespace appkit
