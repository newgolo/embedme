/******************************************************************************
This file is part of AppKit.
Project: appkit
Author : FergusZeng
Email  : cblock@126.com
git	   : https://gitee.com/newgolo/appkit.git
*******************************************************************************
MIT License

Copyright (c) 2022 cblock@126.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
******************************************************************************/
#include "appkit/console.h"

#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "appkit/tracer.h"

namespace appkit {
ArgOption::ArgOption() {}
ArgOption::~ArgOption() {}

bool ArgOption::parseArgs(int argc, char** argv) {
    m_argVect.clear();
    m_optionMap.clear();
    for (auto i = 1; i < argc; i++) {
        m_argVect.emplace_back(std::string(argv[i]));
    }
    std::vector<std::string> valueList;
    std::string option;
    for (auto i = 0; i < m_argVect.size(); i++) {
        std::string arg = m_argVect[i];
        if (arg.size() == 1 && arg[0] == '-') {
            PRINT_ERR("ArgOption, invalid option: %s", CSTR(arg));
            m_argVect.clear();
            m_optionMap.clear();
            return false;
        }
        if (arg.size() >= 2) {
            if (arg[0] == '-') {
                if (!option.empty()) {
                    /* 上一个选项结束 */
                    m_optionMap.insert({option, valueList});
                    option.clear();
                }
                if (arg[1] == '-') {
                    option = arg.substr(2);
                } else {
                    option = arg.substr(1);
                }
                if (option.empty() || !isalpha(option[0]) ||
                    option.find("=") != std::string::npos) {
                    PRINT_ERR("ArgOption invalid option: %s", CSTR(arg));
                    m_argVect.clear();
                    m_optionMap.clear();
                    return false;
                }
                valueList.clear();
                continue;
            }
        }
        if (!option.empty()) {
            valueList.push_back(arg);
        }
    }
    if (!option.empty()) {
        m_optionMap.insert({option, valueList});
    }
    return true;
}

int ArgOption::hasOption(const std::string& option) {
    if (option.empty()) {
        return -1;
    }
    auto iter = m_optionMap.find(option);
    if (iter == m_optionMap.end()) {
        return -1;
    }
    return iter->second.size();
}

std::vector<std::string> ArgOption::getValue(const std::string& option) {
    std::vector<std::string> valueList;
    if (option.empty()) {
        return m_argVect;
    }
    auto iter = m_optionMap.find(option);
    if (iter != m_optionMap.end()) {
        return iter->second;
    }
    return valueList;
}

ConsoleIn::ConsoleIn() {}

ConsoleIn::~ConsoleIn() {}

ConsoleIn& ConsoleIn::waitInput() {
    std::cin >> m_inputStr;
    return *this;
}

bool ConsoleIn::isString(const std::string& str) {
    if (m_inputStr == str) {
        return true;
    }
    return false;
}
int ConsoleIn::size() { return m_inputStr.size(); }

int ConsoleIn::asInt() {
    if (m_inputStr.empty()) {
        return 0;
    }
    return std::stoi(m_inputStr);
}

float ConsoleIn::asFloat() {
    if (m_inputStr.empty()) {
        return 0.0;
    }
    return std::stof(m_inputStr);
}

std::string ConsoleIn::asString() { return m_inputStr; }

const char* ConsoleIn::asCString() { return CSTR(m_inputStr); }

}  // namespace appkit
