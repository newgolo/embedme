/******************************************************************************
This file is part of AppKit.
Project: appkit
Author : FergusZeng
Email  : cblock@126.com
git	   : https://gitee.com/newgolo/appkit.git
*******************************************************************************
MIT License

Copyright (c) 2022 cblock@126.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
******************************************************************************/
#include "appkit/logger.h"

#include "appkit/strutil.h"
#include "appkit/tracer.h"
namespace appkit {
#define LOG_BUFLEN_MAX 1024
#define LOG_ROOT_DIR "/tmp/log/"
Logger::Logger() { m_logBufPtr = std::make_unique<char[]>(LOG_BUFLEN_MAX); }

Logger::~Logger() {}

bool Logger::open(const std::string& logFileName, uint32 maxSize,
                  uint32 rotations) {
    std::lock_guard<std::mutex> lock(m_mutex);
    if (m_file) {
        TRACE_ERR_CLASS("logger is allready open!");
        return false;
    }
    m_file = std::make_shared<File>();
    if (!m_file->open(logFileName, IO_MODE_REWR_ORNEW)) {
        return false;
    }
    m_fileName = logFileName;
    m_maxSize = maxSize;
    m_rotations = rotations;
    return true;
}
void Logger::close() {
    std::lock_guard<std::mutex> lock(m_mutex);
    m_file = nullptr;
}

void Logger::log(const char* format, ...) {
    std::lock_guard<std::mutex> lock(m_mutex);
    va_list argp;
    va_start(argp, format);
    int size = LOG_BUFLEN_MAX - 1;
    char* buf = m_logBufPtr.get();
    size = vsnprintf(buf, size, format, argp);
    va_end(argp);
    if (size > 0) {
        if (size >= LOG_BUFLEN_MAX) { /* 超过长度了 */
            buf[LOG_BUFLEN_MAX - 5] = '.';
            buf[LOG_BUFLEN_MAX - 4] = '.';
            buf[LOG_BUFLEN_MAX - 3] = '.';
            buf[LOG_BUFLEN_MAX - 2] = '\n';
            buf[LOG_BUFLEN_MAX - 1] = 0;
            size = LOG_BUFLEN_MAX;
        }
        int len = m_file->writeData(buf, size);
        if (len > 0) {
            m_writens += len;
        }
        /* 到达最大文件大小,重命名文件并创建新日志文件 */
        if (m_maxSize > 0 && m_writens >= m_maxSize) {
            m_file->close();
            FilePath path(m_fileName);
            std::string dirName = path.dirName();
            std::string fileName = path.baseName();
            std::string suffix = StrUtil::suffix(fileName, ".");
            if (!suffix.empty()) {
                std::string newFile =
                    fileName.substr(0, fileName.size() - suffix.size());
                m_logIdx %= m_rotations;
                // TRACE_DBG_CLASS("fileName:%s, newFile:%s,
                // suffix:%s",CSTR(fileName),CSTR(newFile),CSTR(suffix));
                newFile = StrUtil::format("%s%02d%s", CSTR(newFile), ++m_logIdx,
                                          CSTR(suffix));
                File::renameFile(m_fileName,
                                 dirName.append("/").append(newFile));
                m_writens = 0;
            }
            if (!m_file->open(m_fileName, IO_MODE_REWR_ORNEW)) {
                TRACE_ERR_CLASS("open log file:%s error!", CSTR(m_fileName));
            }
        }
    }
}

LoggerManager::LoggerManager() {}

LoggerManager::~LoggerManager() {
    for (auto kv : m_loggerMap) {
        kv.second->close();
    }
    m_loggerMap.clear();
}

void LoggerManager::setRoot(const std::string& logDir) { m_logDir = logDir; }

bool LoggerManager::createLogger(const std::string& logName, int maxSize,
                                 int rotations) {
    if (m_logDir.empty()) {
        m_logDir = LOG_ROOT_DIR;
    }
    if (Directory::exists(m_logDir) ||
        Directory::createDir(m_logDir, 0666, true)) {
        std::string logFileName =
            m_logDir + std::string("/") + logName + std::string(".log");
        auto iter = m_loggerMap.find(logName);
        if (iter != m_loggerMap.end()) {
            return true;
        }
        auto logger = std::make_shared<Logger>();
        if (!logger->open(logFileName, maxSize, rotations)) {
            TRACE_ERR_CLASS("cannot create log: %s", CSTR(logName));
            return false;
        }
        m_loggerMap.insert(std::make_pair(logName, logger));
        return true;
    }
    TRACE_ERR_CLASS("create log error, no log dir:%s", CSTR(m_logDir));
    return false;
}

std::shared_ptr<Logger> LoggerManager::getLogger(const std::string logName) {
    auto iter = m_loggerMap.find(logName);
    if (iter != m_loggerMap.end()) {
        return iter->second;
    }
    return nullptr;
}
}  // namespace appkit
