/******************************************************************************
This file is part of AppKit.
Project: appkit
Author : FergusZeng
Email  : cblock@126.com
git	   : https://gitee.com/newgolo/appkit.git
*******************************************************************************
MIT License

Copyright (c) 2022 cblock@126.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
******************************************************************************/
#pragma once

#include <iostream>
#include <string>
#include <vector>

#include "appkit/basetype.h"
#include "appkit/regex.h"

/**
 * @file Regex
 * @brief 正则表达式
 */

/*----------------------------------
  RegExp支持的元字符:
  *     匹配0个或多个在*字符前的字符
  .     匹配任意字符
  ^     匹配行首;后面的字符非
  $     匹配行尾
  []    字符集合
  \     转义符
  <>    精确匹配
  {n}   匹配前面的字符出现n次
  {n,}  匹配前面的字符至少出现n次
  {n,m} 匹配前面的字符出现n-m次
  RegExp支持的扩展元字符:
  \w
 ----------------------------------*/
namespace appkit {
/**
 * @class Regex
 * @brief 正则表达式类
 */
class Regex {
    DECL_CLASSMETA(Regex)

public:
    Regex();
    ~Regex();
    /**
     * @brief 正则匹配
     * @param pattern 正则表达式
     * @param source 要匹配的字符串
     * @return true 匹配成功
     * @return false 匹配失败
     */
    bool match(const std::string& pattern, const std::string& source);
    /**
     * @brief 正则匹配
     * @param pattern 正则表达式
     * @param source 要匹配的字符串
     * @param result 首次匹配结果
     * @param pos 首次匹配结果的位置
     * @return true 匹配成功
     * @return false 匹配失败
     */
    bool match(const std::string& pattern, const std::string& source,
               std::string* result, int* pos);
    /**
     * @brief 正则匹配
     * @param pattern 正则表达式
     * @param source 要匹配的字符串
     * @param strArray 匹配结果
     * @param posArray 匹配结果对应位置
     * @param maxMatches 最多匹配个数
     * @return true 匹配成功
     * @return false 匹配失败
     */
    bool match(const std::string& pattern, const std::string& source,
               std::vector<std::string>* strArray, std::vector<int>* posArray,
               int maxMatches = 1);
};
}  // namespace appkit
