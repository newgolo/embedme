/******************************************************************************
This file is part of AppKit.
Project: appkit
Author : FergusZeng
Email  : cblock@126.com
git	   : https://gitee.com/newgolo/appkit.git
*******************************************************************************
MIT License

Copyright (c) 2022 cblock@126.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
******************************************************************************/
#pragma once
#include <getopt.h>

#include <map>
#include <string>
#include <vector>

#include "appkit/basetype.h"

/**
 * @brief 参数工具类头文件
 * @file console.h
 */
namespace appkit {
/**
 *  @file   console.h
 *  @class  ArgOption
 *  @brief  参数解析类(自己实现的不依赖于getopt的解析器)
 */
class ArgOption {
    DECL_CLASSMETA(ArgOption)

public:
    ArgOption();
    ~ArgOption();
    bool parseArgs(int argc, char** argv);
    /**
     * @brief 判断参数是否存在
     * @param option 参数名称
     * @return int 参数值个数(-1:option不存在)
     */
    int hasOption(const std::string& option);
    /**
     * @brief 获取参数值
     * @param option 参数名称
     * @return std::vector<std::string> 返回参数值列表
     * @note 当option为空时,返回的是从argv[1]开始的所有参数列表
     */
    std::vector<std::string> getValue(const std::string& option = "");

private:
    std::map<std::string, std::vector<std::string>> m_optionMap;
    std::vector<std::string> m_argVect;
};

/**
 *  @file   console.h
 *  @class  ConsoleIn
 *  @brief  输入获取类
 */
class ConsoleIn {
    DECL_CLASSMETA(ConsoleIn)

public:
    ConsoleIn();
    virtual ~ConsoleIn();
    /**
     * @brief 等待输入(直至Enter按下才会返回)
     * @return 返回实例引用
     */
    ConsoleIn& waitInput();
    /**
     * @brief 检查输入字符串是否相同
     * @param str
     * @return true 匹配
     * @return false 不匹配
     */
    bool isString(const std::string& str);
    /**
     * @brief 获取输入字符串的长度
     * @return int 字符串长度
     */
    int size();
    /**
     * @brief 将输入字符串转换为整数
     * @return int 字符串字面值
     */
    int asInt();
    /**
     * @brief 将输入字符串转换为浮点数
     * @return float 字符串字面值
     */
    float asFloat();
    /**
     * @brief 返回输入字符串
     * @return std::string 输入字符串
     */
    std::string asString();
    /**
     * @brief 返回C语言格式的输入字符串
     * @return const char* 输入字符串
     */
    const char* asCString();

private:
    std::string m_inputStr{""};
};
}  // namespace appkit
