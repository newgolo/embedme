/******************************************************************************
This file is part of AppKit.
Project: appkit
Author : FergusZeng
Email  : cblock@126.com
git	   : https://gitee.com/newgolo/appkit.git
*******************************************************************************
MIT License

Copyright (c) 2022 cblock@126.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
******************************************************************************/
#pragma once
#include <list>
#include <mutex>
#include <queue>
#include <vector>

namespace appkit {

template <typename T>
class AtomicVector {
public:
    AtomicVector() {}
    AtomicVector(const AtomicVector&) {}
    AtomicVector(const AtomicVector&&) {}
    ~AtomicVector() {}

    bool push_back(const T& ele) {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_vector.emplace_back(ele);
        return true;
    }

    bool pop_back(T* ele) {
        std::lock_guard<std::mutex> lock(m_mutex);
        if (m_vector.empty()) {
            return false;
        }
        *ele = m_vector.back();
        m_vector.pop_back();
        return true;
    }

    T& operator[](size_t index) {
        std::lock_guard<std::mutex> lock(m_mutex);
        return m_vector[index];
    }

    void erase(size_t index) {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_vector.erase(m_vector.begin() + index);
    }

    void clear() {
        std::lock_guard<std::mutex> lock(m_mutex);
        std::vector<T> empty;
        std::swap(empty, m_vector);
    }

    size_t size() {
        std::lock_guard<std::mutex> lock(m_mutex);
        return m_vector.size();
    }

private:
    std::vector<T> m_vector;
    std::mutex m_mutex;
};

template <typename T>
class AtomicList {
public:
    AtomicList() {}
    AtomicList(const AtomicList&) {}
    AtomicList(const AtomicList&&) {}
    ~AtomicList() {}

    bool push_back(const T& ele) {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_list.emplace_back(ele);
        return true;
    }

    bool pop_back(T* ele) {
        std::lock_guard<std::mutex> lock(m_mutex);
        if (m_list.empty()) {
            return false;
        }
        *ele = m_list.back();
        m_list.pop_back();
        return true;
    }

    void clear() {
        std::lock_guard<std::mutex> lock(m_mutex);
        std::vector<T> empty;
        std::swap(empty, m_list);
    }

    size_t size() {
        std::lock_guard<std::mutex> lock(m_mutex);
        return m_list.size();
    }

private:
    std::list<T> m_list;
    std::mutex m_mutex;
};

template <typename T>
class AtomicQueue {
public:
    AtomicQueue() {}
    AtomicQueue(const AtomicQueue&) {}
    AtomicQueue(const AtomicQueue&&) {}
    ~AtomicQueue() {}

    bool push(const T& ele) {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_queue.emplace(ele);
        return true;
    }

    bool pop(T* ele) {
        std::lock_guard<std::mutex> lock(m_mutex);
        if (m_queue.empty()) {
            return false;
        }
        *ele = m_queue.front();
        m_queue.pop();
        return true;
    }

    void clear() {
        std::lock_guard<std::mutex> lock(m_mutex);
        std::queue<T> empty;
        std::swap(empty, m_queue);
    }

    size_t size() {
        std::lock_guard<std::mutex> lock(m_mutex);
        return m_queue.size();
    }

private:
    std::queue<T> m_queue;
    std::mutex m_mutex;
};
}  // namespace appkit