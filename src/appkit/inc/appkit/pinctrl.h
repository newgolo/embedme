/******************************************************************************
This file is part of AppKit.
Project: appkit
Author : FergusZeng
Email  : cblock@126.com
git	   : https://gitee.com/newgolo/appkit.git
*******************************************************************************
MIT License

Copyright (c) 2022 cblock@126.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
******************************************************************************/
#pragma once
#include <string>

#include "appkit/basetype.h"
namespace appkit {
class PinCtrl {
    DECL_CLASSMETA(PinCtrl)

public:
    enum class PinDir { IN = 0, OUT };
    enum class PinVal {
        LOW = 0,
        HIGH,
    };

public:
    /**
     * @brief 创建GPIO控制器
     * @param group GPIO组名,如"PA","PB","GPIOA","GPIOB"等
     * @param pin PIN脚序号
     * @param base GPIO基址
     */
    PinCtrl(const std::string& group, int pin, const std::string& base = "PA");
    virtual ~PinCtrl();
    bool isRequested();
    bool request(const PinDir& dir);
    bool set(const PinVal& val);
    bool get(PinVal* val);
    std::string pinPath();

private:
    int getPinID(const std::string& group, int pin);

private:
    int m_pinID{-1};
    int m_pin{0};
    std::string m_groupBase;
    std::string m_group;
    std::string m_gpioDir;
    std::string m_exportFile;
    std::string m_unexportFile;
    std::string m_directionFile;
    std::string m_valueFile;
};
}  // namespace appkit
