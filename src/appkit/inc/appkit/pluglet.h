/******************************************************************************
This file is part of AppKit.
Project: appkit
Author : FergusZeng
Email  : cblock@126.com
git	   : https://gitee.com/newgolo/appkit.git
*******************************************************************************
MIT License

Copyright (c) 2022 cblock@126.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
******************************************************************************/
#pragma once

#include <map>
#include <string>

#include "appkit/basetype.h"
/**
 * @file pluglet.h
 * @brief 插件
 */
namespace appkit {
/**
 * @enum PlugletType
 * @brief 插件类型
 */
enum class PlugletType : uint8 {
    Lazy = 0, /* 懒惰式加载符号 */
    Now       /* 立即加载所有符号 */
};

/**
 * @class Pluglet
 * @brief 插件类
 */
class Pluglet {
    DECL_CLASSMETA(Pluglet)

public:
    Pluglet();
    ~Pluglet();
    /**
     * @brief 判断插件是否加载
     * @return true
     * @return false
     */
    bool isOpen();
    /**
     * @brief 打开插件
     * @param plugPath
     * @param type
     * @return true
     * @return false
     */
    bool open(std::string plugPath, PlugletType type);
    /**
     * @brief 加载符号
     * @param symName
     * @return true
     * @return false
     */
    bool putSymbol(std::string symName);
    /**
     * @brief 获取符号
     * @param symName
     * @return void*
     */
    void* getSymbol(std::string symName);
    /**
     * @brief 关闭插件
     * @param void
     */
    void close();

private:
    void* m_plugHandle{nullptr};
    std::map<std::string, void*> m_symbolMap;
};
}  // namespace appkit
