#!/bin/bash
USAGE_INFO="[usage] $0 <executable>"
THIS_PATH=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $THIS_PATH
if [ $# -ne 1 ];then
    echo $USAGE_INFO
    exit 1
else
sudo LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$THIS_PATH/lib gdb $1
fi
