#!/bin/bash
THIS_PATH=$PWD
# THIS_PATH=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
if [ -f "$THIS_PATH/install/etc/zeros.sh" -a -d "$THIS_PATH/install/bin" -a -d "$THIS_PATH/install/lib" ]; then
echo -e "\033[33m\033[1m"
cat <<EOF
               _____     ____     _____ 
              |  __ \   / __ \   / ____|
  ____   ___  | |__) | | |  | | | (___  
 |_  /  / _ \ |  _  /  | |  | |  \___ \ 
  / /  |  __/ | | \ \  | |__| |  ____) |
 /___|  \___| |_|  \_\  \____/  |_____/ 
EOF

# toDo:get full path when source this file
export ZEROS_WORKSPACE=$THIS_PATH/install
export LD_LIBRARY_PATH=$ZEROS_WORKSPACE/lib
export PATH=$ZEROS_WORKSPACE/bin:$PATH
source $ZEROS_WORKSPACE/etc/zeros.sh
echo -e "\033[0m"
else
    echo -e "\033[31m\033[1mError: No zeROS workspace found in $THIS_PATH!"
    echo -e 'Please invoke ". install/setup.bash" in a zeROS workspace.\033[0m'
fi
