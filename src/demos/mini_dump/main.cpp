#include <appkit/basetype.h>
#include <appkit/console.h>
#include <appkit/serial.h>
#include <appkit/strutil.h>
#include <appkit/thread.h>
#include <appkit/tracer.h>
#include <client/linux/handler/exception_handler.h>
#include <stdio.h>
#include <string.h>

using namespace appkit;  // NOLINT
static bool dumpCallback(const google_breakpad::MinidumpDescriptor &descriptor,
                         void *context, bool succeeded) {
    TRACE_ERR("dump path: %s", descriptor.path());
    return succeeded;
}

static void crashHare() {
    int *a = nullptr;
    *a = 1;  // 放心的奔溃吧
}

int main(int argc, char **argv) {
    google_breakpad::MinidumpDescriptor descriptor("/var/log/coredump");
    google_breakpad::ExceptionHandler eh(descriptor, NULL, dumpCallback, NULL,
                                         true, -1);
    crashHare();
    return 0;
}
