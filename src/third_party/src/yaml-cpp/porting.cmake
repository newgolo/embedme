cmake_minimum_required(VERSION 3.0)
project(yaml-cpp)

set(PKG_ROOT ${CMAKE_CURRENT_SOURCE_DIR}/v0.7.0)

file(GLOB sources "${PKG_ROOT}/src/[a-zA-Z]*.cpp")
file(GLOB contrib_sources "${PKG_ROOT}/src/contrib/[a-zA-Z]*.cpp")
file(GLOB_RECURSE public_headers "${PKG_ROOT}/include/yaml-cpp/[a-zA-Z]*.h")
file(GLOB private_headers "${PKG_ROOT}/src/[a-zA-Z]*.h")

include_directories(
    ${PKG_ROOT}/include
    ${PKG_ROOT}/src
    ${PKG_ROOT}/src/contrib
)

add_library(${PROJECT_NAME} ${LIB_BUILD_TYPE}
    ${sources}
    ${contrib_sources}
)

install(TARGETS ${PROJECT_NAME}
   DESTINATION ${INSTALL_LIB_PATH}
)