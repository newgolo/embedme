cmake_minimum_required(VERSION 3.0)

project(jsoncpp)

# v1.y.z版本要求编译器必须完全支持C++11
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(PKG_ROOT ${CMAKE_CURRENT_SOURCE_DIR}/v1.9.4)

include_directories(
    ${PKG_ROOT}/include
    ${PKG_ROOT}/include/json
    ${PKG_ROOT}/src/lib_json
)

add_library(${PROJECT_NAME} ${LIB_BUILD_TYPE}
    ${PKG_ROOT}/src/lib_json/json_tool.h
    ${PKG_ROOT}/src/lib_json/json_reader.cpp
    ${PKG_ROOT}/src/lib_json/json_valueiterator.inl
    ${PKG_ROOT}/src/lib_json/json_value.cpp
    ${PKG_ROOT}/src/lib_json/json_writer.cpp
)

install(TARGETS ${PROJECT_NAME}
   DESTINATION ${INSTALL_LIB_PATH}
)

# install(DIRECTORY ${PKG_ROOT}/include/
#     DESTINATION ${INSTALL_INC_PATH}
#     FILES_MATCHING PATTERN "*.h*")