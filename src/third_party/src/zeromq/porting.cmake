cmake_minimum_required(VERSION 3.0)

project(zmq)

set(PKG_ROOT ${CMAKE_CURRENT_SOURCE_DIR}/v4.1.5)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_definitions(-DZMQ_USE_EPOLL=1 -DZMQ_HAVE_UIO)

include_directories(
    ${PKG_ROOT}/include 
)

configure_file(${PKG_ROOT}/builds/cmake/platform.hpp.in ${PKG_ROOT}/src/platform.hpp)
list(APPEND sources ${PKG_ROOT}/src/platform.hpp)

add_library(${PROJECT_NAME} ${LIB_BUILD_TYPE}
    ${PKG_ROOT}/src/address.cpp
    ${PKG_ROOT}/src/clock.cpp
    ${PKG_ROOT}/src/ctx.cpp
    ${PKG_ROOT}/src/curve_client.cpp
    ${PKG_ROOT}/src/curve_server.cpp
    ${PKG_ROOT}/src/dealer.cpp
    ${PKG_ROOT}/src/devpoll.cpp
    ${PKG_ROOT}/src/dist.cpp
    ${PKG_ROOT}/src/epoll.cpp
    ${PKG_ROOT}/src/err.cpp
    ${PKG_ROOT}/src/fq.cpp
    ${PKG_ROOT}/src/io_object.cpp
    ${PKG_ROOT}/src/io_thread.cpp
    ${PKG_ROOT}/src/ip.cpp
    ${PKG_ROOT}/src/ipc_address.cpp
    ${PKG_ROOT}/src/ipc_connecter.cpp
    ${PKG_ROOT}/src/ipc_listener.cpp
    ${PKG_ROOT}/src/kqueue.cpp
    ${PKG_ROOT}/src/lb.cpp
    ${PKG_ROOT}/src/mailbox.cpp
    ${PKG_ROOT}/src/mechanism.cpp
    ${PKG_ROOT}/src/metadata.cpp
    ${PKG_ROOT}/src/msg.cpp
    ${PKG_ROOT}/src/mtrie.cpp
    ${PKG_ROOT}/src/object.cpp
    ${PKG_ROOT}/src/options.cpp
    ${PKG_ROOT}/src/own.cpp
    ${PKG_ROOT}/src/null_mechanism.cpp
    ${PKG_ROOT}/src/pair.cpp
    ${PKG_ROOT}/src/pgm_receiver.cpp
    ${PKG_ROOT}/src/pgm_sender.cpp
    ${PKG_ROOT}/src/pgm_socket.cpp
    ${PKG_ROOT}/src/pipe.cpp
    ${PKG_ROOT}/src/plain_client.cpp
    ${PKG_ROOT}/src/plain_server.cpp
    ${PKG_ROOT}/src/poll.cpp
    ${PKG_ROOT}/src/poller_base.cpp
    ${PKG_ROOT}/src/precompiled.cpp
    ${PKG_ROOT}/src/proxy.cpp
    ${PKG_ROOT}/src/pub.cpp
    ${PKG_ROOT}/src/pull.cpp
    ${PKG_ROOT}/src/push.cpp
    ${PKG_ROOT}/src/random.cpp
    ${PKG_ROOT}/src/raw_encoder.cpp
    ${PKG_ROOT}/src/raw_decoder.cpp
    ${PKG_ROOT}/src/reaper.cpp
    ${PKG_ROOT}/src/rep.cpp
    ${PKG_ROOT}/src/req.cpp
    ${PKG_ROOT}/src/router.cpp
    ${PKG_ROOT}/src/select.cpp
    ${PKG_ROOT}/src/session_base.cpp
    ${PKG_ROOT}/src/signaler.cpp
    ${PKG_ROOT}/src/socket_base.cpp
    ${PKG_ROOT}/src/socks.cpp
    ${PKG_ROOT}/src/socks_connecter.cpp
    ${PKG_ROOT}/src/stream.cpp
    ${PKG_ROOT}/src/stream_engine.cpp
    ${PKG_ROOT}/src/sub.cpp
    ${PKG_ROOT}/src/tcp.cpp
    ${PKG_ROOT}/src/tcp_address.cpp
    ${PKG_ROOT}/src/tcp_connecter.cpp
    ${PKG_ROOT}/src/tcp_listener.cpp
    ${PKG_ROOT}/src/thread.cpp
    ${PKG_ROOT}/src/trie.cpp
    ${PKG_ROOT}/src/v1_decoder.cpp
    ${PKG_ROOT}/src/v1_encoder.cpp
    ${PKG_ROOT}/src/v2_decoder.cpp
    ${PKG_ROOT}/src/v2_encoder.cpp
    ${PKG_ROOT}/src/xpub.cpp
    ${PKG_ROOT}/src/xsub.cpp
    ${PKG_ROOT}/src/zmq.cpp
    ${PKG_ROOT}/src/zmq_utils.cpp
    ${PKG_ROOT}/src/config.hpp
)

install(TARGETS ${PROJECT_NAME}
   DESTINATION ${INSTALL_LIB_PATH}
)

# install(DIRECTORY ${PKG_ROOT}/include/
#     DESTINATION ${INSTALL_INC_PATH}
#     FILES_MATCHING PATTERN "*.h*")