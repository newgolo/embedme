# 设置third_party相关变量及搜索目录
include(${PKG_THIRD_PARTY_PATH}/src/fmt/package.cmake)
include(${PKG_THIRD_PARTY_PATH}/src/json/package.cmake)
include(${PKG_THIRD_PARTY_PATH}/src/spdlog/package.cmake)
include(${PKG_THIRD_PARTY_PATH}/src/sqlite_orm/package.cmake)
include(${PKG_THIRD_PARTY_PATH}/src/tinyfsm/package.cmake)
include(${PKG_THIRD_PARTY_PATH}/src/yaml-cpp/package.cmake)
include(${PKG_THIRD_PARTY_PATH}/src/zeromq/package.cmake)
