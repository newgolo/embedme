###############################################################################
# 目标平台: aarch64 ubuntu20.04LTS
# 编译平台: x64_86 ubuntu20.04LTS
###############################################################################
include(CMakeForceCompiler)
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR aarch64)
set(CMAKE_LIBRARY_ARCHITECTURE aarch64-linux-gnu)

# 配置交叉编译环境
set(TOOLCHAIN_PATH /opt/toolchain/linux-aarch64-noetic)
set(SYSROOT_PATH ${TOOLCHAIN_PATH}/sysroot)
set(TRIPLET aarch64-linux-gnu)
set(CMAKE_C_COMPILER /usr/bin/${TRIPLET}-gcc)
set(CMAKE_CXX_COMPILER /usr/bin/${TRIPLET}-g++)

# set(CMAKE_FIND_ROOT_PATH  ${SYSROOT_PATH} ${SYSROOT_PATH}/opt/ros/noetic)
set(CMAKE_FIND_ROOT_PATH  ${SYSROOT_PATH})
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)

# 设置特定cmake变量
set(CMAKE_SIZEOF_VOID_P 8)
set(CMAKE_CROSSCOMPILING true)

# 设置SYSROOT:设置后可能会有编译时找不到库的问题
set(CMAKE_SYSROOT ${SYSROOT_PATH})
set(CMAKE_SYSROOT_COMPILE ${SYSROOT_PATH})

# 修改CMake默认变量值(请谨慎修改!!!)
# set(ENV{SYSROOT_DIR} ${SYSROOT_PATH})
# set(CMAKE_INSTALL_PREFIX ${SYSROOT_PATH}/opt/ros/noetic)
# set(CMAKE_STAGING_PREFIX ${SYSROOT_PATH})
# set(CMAKE_INCLUDE_PATH ${SYSROOT_PATH}/usr/include)
# set(CMAKE_LIBRARY_PATH ${SYSROOT_PATH}/usr/lib)

# 设置第三方工具
set(ENV{PKG_CONFIG_PATH} ${SYSROOT_PATH}/usr/lib/pkgconfig)
set(PYTHON_EXECUTABLE /usr/bin/python3)
set(ENV{PROTOC_INSTALL_PATH} /usr/bin)

# 添加头文件及库搜索路径 
include_directories(${SYSROOT_PATH}/usr/include/aarch64-linux-gnu)
list(APPEND CMAKE_PREFIX_PATH /usr/lib/aarch64-linux-gnu/)
list(APPEND CMAKE_MODULE_PATH /usr/lib/aarch64-linux-gnu/cmake)
