# 设置prebuilt相关变量及搜索目录
set(PREBUILT_PATH ${WS_ROOT}/prebuilt/$ENV{ZBUILD_PLATFORM})
set(PREBUILT_BIN_PATH ${PREBUILT_PATH}/bin)
set(PREBUILT_LIB_PATH ${PREBUILT_PATH}/lib)
set(PREBUILT_INC_PATH ${PREBUILT_PATH}/include)

# 设置预编译软件包路径
include_directories(${WS_ROOT}/prebuilt/include)
include_directories(${WS_ROOT}/prebuilt/include/breakpad)
include_directories(${PREBUILT_INC_PATH})
link_directories(${PREBUILT_LIB_PATH})

# 安装预编译动态库
install(DIRECTORY ${PREBUILT_LIB_PATH}
    DESTINATION ${WS_OUTPUT}/install
    FILES_MATCHING PATTERN "*.so*"
    # USE_SOURCE_PERMISSIONS
)
