#!/bin/bash
THIS_PATH=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
###########################################################
# 编译设置
BUILD_RELEASE=yes   # yes/no
BUILD_PLATFORM=0    # 0:linux-amd64 1:linux-arm64 2:windows-x64
###########################################################
# 设置环境变量
cd $THIS_PATH
export WS_ROOT=$THIS_PATH
export PROTOC_INSTALL_PATH=/usr/bin
echo ">>> Build workspace in non-ROS environment."

# 配置运行环境
source $WS_ROOT/environ/envsetup.sh
zbuild_setup $BUILD_PLATFORM $BUILD_RELEASE

# 编译工程
cd $WS_ROOT/zbuild
BUILD_PATH=build-$ZBUILD_PLATFORM
mkdir -p $BUILD_PATH
cd $BUILD_PATH
cmake ..
make install
